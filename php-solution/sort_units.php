<?php

if (in_array('--help', $argv) || in_array('-h', $argv)) {
    // display help and exit
    echo "Usage:\n";
    echo "  php {$argv[0]} [input file]\n\n";
    echo "  [input file] defaults to stdin\n";
    exit;
}

$input = (count($argv) < 2 || $argv[1] == '-') ? 'php://stdin' : $argv[1];

$lines = file($input, FILE_SKIP_EMPTY_LINES);

/**
 * Matches a line to the specified input.
 *
 * @param  string     $line
 * @return bool|array false, if no match, matched array otherwise
 */
function match_line($line) {
    if (!preg_match('/^#([0-9]+)[A-Z]* - (.*)/', $line, $m)) {
        return false;
    }
    return $m;
}

// First, sort the lines.
usort($lines, function ($a, $b) {
    $am = match_line($a);
    $bm = match_line($b);

    if (!$am && !$bm) {
        return 0;
    }

    if (!$am) {
        return -1;
    }

    if (!$bm) {
        return 1;
    }

    return $am[1] - $bm[1];
});

// Next, filter out invalid lines.
$lines = array_filter($lines, function ($line) {
    return (bool) match_line($line);
});

// Finally, output them, one at a time.
// file left the newlines in, so I don't have to worry about that.
foreach($lines as $line) {
    echo $line;
}
