# Unit Number Sorting Exercise

## Assignment

The task is to sort lease data read from a file and to print the sorted data to STDOUT. The data looks as follows
(sample also attached):

    #50 - Smith
    #8 - Johnson
    #100 - Sanders
    #1B - Adams
    #1A - Kessenich

Each line contains a unit number and a resident name. The data should be sorted by unit number.

Develop a solution in PHP and one other language of your choice that reads the data from a file and prints the
data (sorted by unit number) to STDOUT. The printed strings should not be modified from how they appear in
the input file.

## Solution

First, I created a [PHP script](generate/generate.php) that would actually generate the data. I used the [Faker](https://github.com/fzaninotto/Faker/) library for generation of random data.

Second, the [PHP script](php-solution/sort_units.php) takes in a file as the first command-line argument (or reads from `stdin`). It uses a simple regular expression to match the unit number in the line, and uses a custom sorting function to sort based on that number.

Third, the [JS script](js-solution/index.js) is a node app (since browser-based JS can't normally read from the filesystem). It uses the built-in node libraries `fs` and `readline` to create a stream from the file given as the first command-line argument (or uses `stdin` if no file is supplied). It reads each line, adding it to an array. It then sorts that array, using a nearly identical sorting method as the PHP script (and the same regular expression), and outputs each line.

Both scripts only output lines that match the regular expression. In this case, if invalid data is passed in, it will be silently ignored.

Each PHP script was tested with PHP 7 (although should work fine on PHP 5). Node 7.7.3 was used for the JS script.
