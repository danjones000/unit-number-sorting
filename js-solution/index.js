/* global require, console, process */

const fs = require('fs');
const readline = require('readline');

process.argv.has = function(item) {
	return this.indexOf(item) > -1;
};

if (process.argv.has('--help') || process.argv.has('-h') || process.argv.has('help')) {
	console.log('Usage:');
	console.log(`  node ${process.argv[1]} [input]`);
	console.log('  [input] defaults to stdin');
	process.exit();
}

const input = process.argv[2] ? fs.createReadStream(process.argv[2]) : process.stdin;
const rl = readline.createInterface({input: input});
let lines = [];
const reg = /^#([0-9]+)[A-Z]* - (.*)/;

rl.on('line', function(line) {
	lines.push(line);
});

rl.on('close', function () {
	lines.sort(function (a, b) {
		const am = reg.exec(a);
		const bm = reg.exec(b);

		if (!am && !bm) return 0;
		if (!am) return -1;
		if (!bm) return 1;
		return Number(am[1]) - Number(bm[1]);
	});
	lines.forEach(function (line) {
		if (reg.test(line)) console.log(line);
	});
});
