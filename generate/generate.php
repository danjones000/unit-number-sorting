<?php

/**
 * This is a simple command-line script that generates a list of fake tenants, in the following format:
 *   #<unit_number> - <last_name>
 */

require_once 'vendor/autoload.php';

use Faker\Factory as FakerFactory;

$faker = FakerFactory::create();

if (in_array('--help', $argv) || in_array('-h', $argv)) {
    // display help and exit
    echo "Usage:\n";
    echo "  {$argv[0]} [number of entries] [output file]\n\n";
    echo "  [number of entries] defaults to 50\n";
    echo "  [output file] defaults to stdout\n";
    exit;
}

// The first argument should contain the number of entries. Default to 50.
$count = (count($argv) < 2 || !is_numeric($argv[1])) ? 50 : (int) $argv[1];
$output = (count($argv) < 3 || $argv[2] == '-') ? 'php://stdout' : $argv[2];
$stdout = (in_array($output, ['php://stdout', '/dev/stdout'])) ? 'php://stderr' : 'php://stdout';

file_put_contents($stdout, "Writing $count entries to $output\n");

$checkwrite = file_exists($output) ? $output : dirname($output);

if ($output != 'php://stdout' && !is_writable($checkwrite)) {
    file_put_contents('php://stderr', "Cannot write to {$checkwrite}\n");
    exit(1);
}

$fp = fopen($output, 'w');
for($i = 0; $i < $count; $i++) {
    $unit = $faker->randomElement(array_merge(range('A','E'),['']));
    fwrite($fp, "#{$faker->buildingNumber}{$unit} - {$faker->lastName}\n");
}
fclose($fp);

file_put_contents($stdout, "Completed writing to $output\n");